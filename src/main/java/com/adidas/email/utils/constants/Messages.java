package com.adidas.email.utils.constants;

public class Messages {
	public static final String ERROR = "Error: %s";
	public static final String TOPIC_MAIL_CONTENT_NOT_FOUND = "The email topic subject or message was not found.";
	public static final String MAIL_MESSAGE_GENERATION_EXCEPTION = "The email could not be generated.";
	public static final String SEND_EMAIL_EXCEPTION = "The email could not be sent.";
}
