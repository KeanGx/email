package com.adidas.email.utils.enums;

public enum EmailTopic {
	SUBSCRIPTION_CREATED, SUBSCRIPTION_CANCELED;
}
