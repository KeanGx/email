package com.adidas.email.controllers;

import com.adidas.email.dtos.requests.EmailNotificationRequest;
import com.adidas.email.dtos.responses.ApiResponseDTO;
import com.adidas.email.exceptions.SendEmailException;
import com.adidas.email.services.EmailService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
public class EmailController {

	@Autowired
	private EmailService emailService;

	@ApiOperation(
			value = "Send Email Notification for Topic",
			tags = {"Email Service"})
	@ApiResponses(
			value = {
					@ApiResponse(code = 202, message = "Request to send email notification for Topic was accepted.", response = ApiResponseDTO.class),
					@ApiResponse(
							code = 400,
							message = "Returns ApiResponseDTO object with cause details",
							response = ApiResponseDTO.class)
			})
	@PostMapping(value = "/v1.0/notifications", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ApiResponseDTO<Long>> sendNotificationEmail(@RequestBody @Valid EmailNotificationRequest emailNotificationRequest) {
		ApiResponseDTO<Long> response;
		try {
			emailService.sendEmailNotification(emailNotificationRequest);
			return ResponseEntity.status(HttpStatus.OK).build();
		} catch (SendEmailException exception) {
			response = new ApiResponseDTO<>(HttpStatus.BAD_REQUEST, exception);
			return ResponseEntity.status(response.getStatus()).body(response);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}

	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ApiResponseDTO handleValidationExceptions(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});

		return new ApiResponseDTO<Map>(HttpStatus.BAD_REQUEST, MethodArgumentNotValidException.class.getSimpleName(), errors);
	}
}
