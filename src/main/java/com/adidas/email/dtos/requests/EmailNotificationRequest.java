package com.adidas.email.dtos.requests;

import com.adidas.email.utils.enums.EmailTopic;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailNotificationRequest {

	@Email(message = "Email is required and follow the correct format of an email")
	@ApiModelProperty(required = true)
	private String email;

	@NotNull(message = "Email Topic is required.")
	@ApiModelProperty(required = true)
	private EmailTopic topic;

}
