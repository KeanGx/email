package com.adidas.email.exceptions;

import com.adidas.email.utils.constants.Messages;

import static java.lang.String.format;

public class SendEmailException extends Exception {
	private static final String MESSAGE = Messages.SEND_EMAIL_EXCEPTION;
	private static final String ERROR = Messages.ERROR;

	public SendEmailException() {
		super(MESSAGE);
	}

	public SendEmailException(String error) {
		super(MESSAGE.concat(format(ERROR, error)));
	}

}
