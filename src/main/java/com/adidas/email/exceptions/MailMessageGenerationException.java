package com.adidas.email.exceptions;

import com.adidas.email.utils.constants.Messages;

public class MailMessageGenerationException extends Exception {
	private static final String MESSAGE = Messages.MAIL_MESSAGE_GENERATION_EXCEPTION;

	public MailMessageGenerationException() {
		super(MESSAGE);
	}

}
