package com.adidas.email.exceptions;

import com.adidas.email.utils.constants.Messages;

import static java.lang.String.format;

public class TopicMailContentNotFound extends Exception {
	private static final String MESSAGE = Messages.TOPIC_MAIL_CONTENT_NOT_FOUND;
	private static final String ERROR = Messages.ERROR;

	public TopicMailContentNotFound() {
		super(MESSAGE);
	}

	public TopicMailContentNotFound(String error) {
		super(MESSAGE.concat(format(ERROR, error)));
	}

}
