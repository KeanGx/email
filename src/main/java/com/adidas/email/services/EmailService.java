package com.adidas.email.services;

import com.adidas.email.clients.JavaMailSender;
import com.adidas.email.dtos.requests.EmailNotificationRequest;
import com.adidas.email.exceptions.MailMessageGenerationException;
import com.adidas.email.exceptions.SendEmailException;
import com.adidas.email.exceptions.TopicMailContentNotFound;
import com.adidas.email.utils.enums.EmailTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.NoSuchMessageException;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Locale;

@Service
@Validated
public class EmailService implements Serializable {

	@Autowired
	private JavaMailSender emailSender;

	@Autowired
	private MessageService messageService;

	public static final String EMAIL_SUBJECT_SUFFIX = "_SUBJECT";
	public static final String EMAIL_MESSAGE_SUFFIX = "_MESSAGE";
	public static final Locale defaultLocale = Locale.ENGLISH;

	private final Logger log = LoggerFactory.getLogger(EmailService.class);

	public void sendEmailNotification(@NotNull @Valid EmailNotificationRequest notificationRequest) throws SendEmailException {
		try {
			SimpleMailMessage mailMessage = generateEmailForTopic(notificationRequest.getEmail(), notificationRequest.getTopic());
			emailSender.send(mailMessage);
		} catch (Exception e) {
			log.error("The email could not be sent. Email: {}, Topic: {}. Error: {}", notificationRequest.getEmail(), notificationRequest.getTopic(), e.getLocalizedMessage());
			throw new SendEmailException(e.getLocalizedMessage());
		}
	}

	protected SimpleMailMessage generateEmailForTopic(@NotBlank String email, @NotNull EmailTopic topic) throws TopicMailContentNotFound, MailMessageGenerationException {
		SimpleMailMessage mailMessage = null;
		try {
			String subject = messageService.getMessage(topic.name().concat(EMAIL_SUBJECT_SUFFIX), defaultLocale.getLanguage());
			String message = messageService.getMessage(topic.name().concat(EMAIL_MESSAGE_SUFFIX), defaultLocale.getLanguage());

			mailMessage = new SimpleMailMessage();
			mailMessage.setFrom("noreply.newsletter@adidas.com");
			mailMessage.setTo(email);
			mailMessage.setSubject(subject);
			mailMessage.setText(message);
		} catch (NoSuchMessageException e) {
			throw new TopicMailContentNotFound(e.getLocalizedMessage());
		} catch (Exception e) {
			log.error("The email could not be generated. Email: {}, Topic: {}. Error: {}", email, topic, e.getLocalizedMessage());
			throw new MailMessageGenerationException();
		}

		return mailMessage;
	}
}
