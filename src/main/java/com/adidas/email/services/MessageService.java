package com.adidas.email.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Locale;

@Service
public class MessageService implements Serializable {

	@Autowired
	private transient MessageSource messageSource;
	
	public String getMessage(String code, String language) {
		return messageSource.getMessage(code, null, new Locale(language));
	}
}
