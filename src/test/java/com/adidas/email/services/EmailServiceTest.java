package com.adidas.email.services;

import com.adidas.email.BaseTestClass;
import com.adidas.email.clients.JavaMailSender;
import com.adidas.email.dtos.requests.EmailNotificationRequest;
import com.adidas.email.exceptions.MailMessageGenerationException;
import com.adidas.email.exceptions.SendEmailException;
import com.adidas.email.exceptions.TopicMailContentNotFound;
import com.adidas.email.utils.enums.EmailTopic;
import org.apache.commons.collections.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.NoSuchMessageException;
import org.springframework.mail.SimpleMailMessage;

import javax.validation.ConstraintViolationException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class EmailServiceTest extends BaseTestClass {

	@Autowired
	private EmailService emailService;

	@SpyBean
	private MessageService messageService;

	@MockBean
	private JavaMailSender emailSender;

	@Test
	void sendEmailNotification() throws SendEmailException {
		EmailNotificationRequest request = new EmailNotificationRequest();
		request.setEmail("email@adidas.com");
		request.setTopic(EmailTopic.SUBSCRIPTION_CANCELED);

		ArgumentCaptor<SimpleMailMessage> mailMessageArgumentCaptor = forClass(SimpleMailMessage.class);

		emailService.sendEmailNotification(request);

		verify(emailSender, times(1)).send(mailMessageArgumentCaptor.capture());

		SimpleMailMessage mailSent = mailMessageArgumentCaptor.getValue();
		assertNotNull(mailSent);
		assertNotNull(mailSent.getTo());
		assertAll("mailMessage",
				() -> Assertions.assertTrue(asList(mailSent.getTo()).contains(request.getEmail())),
				() -> Assertions.assertNotNull(mailSent.getSubject()),
				() -> Assertions.assertNotNull(mailSent.getText())
		);

	}

	@Test
	void sendEmailNotification_validationFails() {
		EmailNotificationRequest request = new EmailNotificationRequest();
		request.setEmail("email.adidas.com");
		request.setTopic(EmailTopic.SUBSCRIPTION_CANCELED);

		assertThrows(ConstraintViolationException.class, () -> emailService.sendEmailNotification(request));
	}

	@Test
	void sendEmailNotification_throwsSendEmailException() {
		Locale defaultLocale = Locale.ENGLISH;
		EmailNotificationRequest request = new EmailNotificationRequest();
		request.setEmail("email@adidas.com");
		request.setTopic(EmailTopic.SUBSCRIPTION_CANCELED);

		doThrow(new NoSuchMessageException("SUBSCRIPTION_CREATED_SUBJECT", defaultLocale))
				.when(messageService).getMessage(any(), eq(defaultLocale.getLanguage()));

		assertThrows(SendEmailException.class, () -> emailService.sendEmailNotification(request));
	}

	@Test
	void generateEmailForTopic() throws Exception {
		String email = "email@adidas.com";
		EmailTopic topic = EmailTopic.SUBSCRIPTION_CREATED;

		SimpleMailMessage mailMessage = emailService.generateEmailForTopic(email, topic);
		assertNotNull(mailMessage);
		assertNotNull(mailMessage.getTo());
		assertAll("mailMessage",
				() -> Assertions.assertTrue(asList(mailMessage.getTo()).contains(email)),
				() -> Assertions.assertNotNull(mailMessage.getSubject()),
				() -> Assertions.assertNotNull(mailMessage.getText())
		);
	}

	@Test
	void generateEmailForTopic_throwsTopicMailContentNotFound() {
		Locale defaultLocale = Locale.ENGLISH;
		String email = "email@adidas.com";
		EmailTopic topic = EmailTopic.SUBSCRIPTION_CREATED;

		doThrow(new NoSuchMessageException("SUBSCRIPTION_CREATED_SUBJECT", defaultLocale))
				.when(messageService).getMessage(any(), eq(defaultLocale.getLanguage()));

		assertThrows(TopicMailContentNotFound.class, () -> emailService.generateEmailForTopic(email, topic));
	}

	@Test
	void generateEmailForTopic_throwsMailMessageGenerationException() {
		Locale defaultLocale = Locale.ENGLISH;
		String email = "email@adidas.com";
		EmailTopic topic = EmailTopic.SUBSCRIPTION_CREATED;

		doThrow(new NullPointerException())
				.when(messageService).getMessage(any(), eq(defaultLocale.getLanguage()));

		assertThrows(MailMessageGenerationException.class, () -> emailService.generateEmailForTopic(email, topic));
	}
}